//
//  BlessingView.swift
//  BlessingBook
//
//  Created by Namthip Silsuwan on 19/12/2563 BE.
//

import SwiftUI

struct BlessingView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var points: [[CGPoint]] = []
    
    @State var currentPoint: [CGPoint] = []
    
    var numberOfPath = 0
    
    var body: some View {
        
        ZStack {
            LinearGradient(
                gradient: Gradient(colors: [Color("pinkgold"), Color("teal")]), startPoint: .leading, endPoint: .trailing)
            
            ZStack {
                Rectangle()
                    .foregroundColor(.white)
                    .padding(20)
                    .opacity(/*@START_MENU_TOKEN@*/0.8/*@END_MENU_TOKEN@*/)
                    .gesture(DragGesture().onChanged( { value in
                        self.addNewPoint(value)
                    })
                    .onEnded( { value in
                        points.append(currentPoint)
                    }))
                
                    DrawShape(points: currentPoint)
                        .stroke(lineWidth: 5)
                        .foregroundColor(Color("sweetgray"))
            }
            
        }.edgesIgnoringSafeArea(.all)
        
    }
    
    private func addNewPoint(_ value: DragGesture.Value) {
        currentPoint.append(value.location)
    }
}

struct DrawShape: Shape {
    
    var points: [CGPoint]
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        guard let firstPoint = points.first else { return path }
        
        path.move(to: firstPoint)
        for pointIndex in 1..<points.count {
            path.addLine(to: points[pointIndex])
            
        }
        return path
    }
}

struct BlessingView_Previews: PreviewProvider {
    static var previews: some View {
        BlessingView().previewLayout(.fixed(width: 1024, height: 768))
    }
}
