//
//  ColorExtension.swift
//  BlessingBook
//
//  Created by Namthip Silsuwan on 31/12/2563 BE.
//

import SwiftUI

extension Color {
    static let pinkgold = Color("pinkgold")
    static let orange1 = Color("orange1")
    static let orange2 = Color("orange2")
    static let brown = Color("brown")
    static let pink1 = Color("pink1")
    static let pinkred = Color("pinkred")
    static let redpurple = Color("redpurple")
    static let greypurple = Color("greypurple")
    static let purple = Color("purple")
    static let sweetgray = Color("sweetgray")
    static let blue1 = Color("blue1")
    static let blue2 = Color("blue2")
    static let blue3 = Color("blue3")
    static let blueocean = Color("blueocean")
    static let deepteal = Color("deepteal")
    static let teal = Color("teal")
    
}
