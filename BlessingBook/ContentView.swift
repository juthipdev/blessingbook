//
//  ContentView.swift
//  BlessingBook
//
//  Created by Namthip Silsuwan on 18/12/2563 BE.
//

import SwiftUI

fileprivate let fontSize: CGFloat = 110

struct ContentView: View {
    @State private var presented = false
    
    @State private var brideView = BrideView()
    @State private var groomView = GroomView()
    
    @State private var heartScale: CGFloat = 1
    @State private var heartOutlineScale: CGFloat = 0
    
    @State private var brideViewOffset: CGFloat = -280
    @State private var groomViewOffset: CGFloat = 280
    
    @State private var brideGroomOpacity: Double = 1
    @State private var mergeNameOpacity: Double = 0
    
    
    
    var body: some View {
        ZStack {
            
            LinearGradient(
                gradient: Gradient(colors: [Color("pinkgold"), Color("teal")]), startPoint: .leading, endPoint: .trailing)
            HStack {
                brideView
                    .offset(x: brideViewOffset, y: 0)
                    .opacity(brideGroomOpacity)
                groomView
                    .offset(x: groomViewOffset, y: 0)
                    .opacity(brideGroomOpacity)
            }
            VStack {
                Image("heart")
                    .padding(100)
                    .scaleEffect(heartScale)
                Rectangle().foregroundColor(.clear)
            }
            
            VStack {
                Image("heart-shape-outline")
                    .padding(100)
                    .scaleEffect(heartOutlineScale)
                Rectangle().foregroundColor(.clear)
            }
            
            VStack {
                Text("Nam  thip  atai")
                    .font(
                        .custom("Adelio Darmanto",size: fontSize)
                    )
                    .foregroundColor(.white)
                    .padding(150)
                    .opacity(mergeNameOpacity)
                Rectangle().foregroundColor(.clear).opacity(brideGroomOpacity)
            }
            
            Image("wedding-couple")
            
            VStack {
                Rectangle().foregroundColor(Color.clear)
                Button(action: {
                    self.presented.toggle()
                }, label: {
                    Text("START")
                        .font(.custom("Adelio Darmanto",size: 40))
                        .foregroundColor(.white)
                        .padding(20)
                        .cornerRadius(50)
                })
//                .border(Color.white, width: 10)
//                .cornerRadius(50)
                
                Rectangle().foregroundColor(Color.clear).frame(width: 100, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }
            
            .frame(minWidth: 500, minHeight: 500)
            .fullScreenCover(isPresented: $presented, content: {
                BlessingView2()
            })
            
            Rectangle()
                .border(Color.white, width: 16)
                .padding(20).foregroundColor(.clear)
            
        }.edgesIgnoringSafeArea(.all)
        .statusBar(hidden: true)
        .onAppear {
            let baseAnimation = Animation.easeInOut(duration: 2)
            return withAnimation(baseAnimation) {
                self.heartScale = 0
                self.heartOutlineScale = 1
                self.brideViewOffset = 280
                self.groomViewOffset = -280
                self.brideGroomOpacity = 0
            }
        }.onAppear {
            let animation = Animation.easeInOut(duration: 3)
            return withAnimation(animation) {
                self.mergeNameOpacity = 1
            }
        }
    }
}

struct BrideView: View {
    var body: some View {
        ZStack {
            VStack {
                Text("Namthip")
                    .font(
                        .custom("Adelio Darmanto",size: fontSize)
                    )
                    .foregroundColor(.white)
                    .padding(150)
                Rectangle().foregroundColor(.clear)
            }
            
        }
    }
}

struct GroomView: View {
    var body: some View {
        ZStack {
            VStack {
                Text("Tipatai")
                    .font(
                        .custom("Adelio Darmanto",size: fontSize)
                    )
                    .foregroundColor(.white)
                    .padding(150)
                Rectangle().foregroundColor(.clear)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView().previewLayout(.fixed(width: 1194, height: 834))
        }
    }
}
