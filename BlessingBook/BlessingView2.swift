//
//  BlessingView2.swift
//  BlessingBook
//
//  Created by Namthip Silsuwan on 19/12/2563 BE.
//

import SwiftUI
import FirebaseStorage

struct BlessingView2: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State private var currentDrawing: Drawing = Drawing()
    @State private var drawings: [Drawing] = [Drawing]()
    @State private var lineColor: Color = Color.sweetgray
    @State private var lineWidth: CGFloat = 8.0
    @State private var uploading: Bool = false
    
    let imageSize: CGSize = CGSize(width: 1326, height: 856)
    let storage = Storage.storage()
    
    var body: some View {
        
        ZStack {
            LinearGradient(
                gradient: Gradient(colors: [Color.pinkgold, Color.teal]), startPoint: .leading, endPoint: .trailing)

            ZStack {
                VStack {
                    drawingView
                    buttonPanelView.padding(.bottom, 20)
                }
                loadingView
            }

        }.edgesIgnoringSafeArea(.bottom)
        
    }
    
    var drawingView: some View {
        GeometryReader { geometry in
                Path { path in
                    for drawing in self.drawings {
                        self.add(drawing: drawing, toPath: &path)
                    }
                    self.add(drawing: self.currentDrawing, toPath: &path)
                }
                .stroke(lineColor, lineWidth: lineWidth)
                    .background(Color(white: 0.95))
                    .cornerRadius(16)
                    .gesture(
                        DragGesture(minimumDistance: 0.1)
                            .onChanged({ (value) in
                                let currentPoint = value.location
                                if currentPoint.y >= 0
                                    && currentPoint.y < geometry.size.height {
                                    self.currentDrawing.points.append(currentPoint)
                                }
                            })
                            .onEnded({ (value) in
                                self.drawings.append(self.currentDrawing)
                                self.currentDrawing = Drawing()
                            })
                )
        }
        .foregroundColor(.white)
        .padding(20)
        .opacity(0.9)
        
    }
    
    var buttonPanelView: some View {
        
        ZStack {
            
            Color(white: 0.95)
                .cornerRadius(16)
            
            HStack {
                colorView
                
                Spacer ()
                
                Text("'\n'\n'")
                    .foregroundColor(Color.blueocean)
                
                Button(action: {
                    self.drawings.removeAll()
                }, label: {
                    Text("CLEAR")
                        .font(.custom("Adelio Darmanto",size: 40))
                        .foregroundColor(Color.blueocean)
                        .padding(20)
                        .cornerRadius(50)
                })
                
                Button(action: {
                    saveAndUploadImage()
                }, label: {
                    Text("SAVE")
                        .font(.custom("Adelio Darmanto",size: 40))
                        .foregroundColor(Color.blueocean)
                        .padding(20)
                        .cornerRadius(50)
                }).padding(.trailing, 20)
            }
        }
        .frame(height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
        .padding(.leading, 20)
        .padding(.trailing, 20)
    }
    
    var loadingView: some View {
        Circle()
            .trim(from: 0, to: 0.7)
            .stroke(Color.pinkgold, lineWidth: 5)
            .frame(width: 100, height: 100)
            .rotationEffect(Angle(degrees: uploading ? 360 : 0))
            .animation(Animation.default.repeatForever(autoreverses: false))
            .opacity(uploading ? 1.0 : 0.0)
    }
    
    var colorView: some View {
        HStack {
            
            HStack {
                Button("", action: {
                    self.lineColor = Color.orange1
                }).buttonStyle(ColorPalatteStyle(color: Color.orange1))
                
                Button("", action: {
                    self.lineColor = Color.orange2
                }).buttonStyle(ColorPalatteStyle(color: Color.orange2))
                
                Button("", action: {
                    self.lineColor = Color.brown
                }).buttonStyle(ColorPalatteStyle(color: Color.brown))
            }
            
            HStack {
                Button("", action: {
                    self.lineColor = Color.pink1
                }).buttonStyle(ColorPalatteStyle(color: Color.pink1))
                
                Button("", action: {
                    self.lineColor = Color.pinkred
                }).buttonStyle(ColorPalatteStyle(color: Color.pinkred))
                
                Button("", action: {
                    self.lineColor = Color.redpurple
                }).buttonStyle(ColorPalatteStyle(color: Color.redpurple))
            }
        
            HStack {
                Button("", action: {
                    self.lineColor = Color.greypurple
                }).buttonStyle(ColorPalatteStyle(color: Color.greypurple))
                
                Button("", action: {
                    self.lineColor = Color.purple
                }).buttonStyle(ColorPalatteStyle(color: Color.purple))
                
                Button("", action: {
                    self.lineColor = Color.sweetgray
                }).buttonStyle(ColorPalatteStyle(color: Color.sweetgray))
            }
            
            HStack {
                Button("", action: {
                    self.lineColor = Color.blue1
                }).buttonStyle(ColorPalatteStyle(color: Color.blue1))
                
                Button("", action: {
                    self.lineColor = Color.blue2
                }).buttonStyle(ColorPalatteStyle(color: Color.blue2))
                
                Button("", action: {
                    self.lineColor = Color.blue3
                }).buttonStyle(ColorPalatteStyle(color: Color.blue3))
                Button("", action: {
                    self.lineColor = Color.deepteal
                }).buttonStyle(ColorPalatteStyle(color: Color.deepteal))
            }
        }
    }
    
    private func add(drawing: Drawing, toPath path: inout Path) {
        let points = drawing.points
        if points.count > 1 {
            for i in 0..<points.count-1 {
                let current = points[i]
                let next = points[i+1]
                path.move(to: current)
                path.addLine(to: next)
            }
        }
    }
    
    private func saveAndUploadImage() {
        let blessingImage = drawingView.asImage(size: imageSize)
        UIImageWriteToSavedPhotosAlbum(blessingImage, nil, nil, nil)
        guard let imageData = blessingImage.jpegData(compressionQuality: 0) else {
            return
        }
        
        let storageRef = storage.reference()
        let currentTime = Int(Date().timeIntervalSince1970)
        let blessingRef = storageRef.child("images/blessing_\(currentTime).jpg")
        uploading = true
        blessingRef.putData(imageData, metadata: nil) { (metadata, error) in
            self.uploading = false
            guard metadata != nil else {
                // Uh-oh, an error occurred!
                return
            }
            self.presentationMode.wrappedValue.dismiss()
        }
    }

}

struct ColorPalatteStyle: ButtonStyle {
    
    var color: Color
 
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: .infinity)
            .frame(width: 50, height: 50)
            .background(color)
            .clipShape(Circle())
            .padding(.leading, 20)
    }
}

struct Drawing {
    var points: [CGPoint] = [CGPoint]()
}

struct BlessingView2_Previews: PreviewProvider {
    static var previews: some View {
        BlessingView().previewLayout(.fixed(width: 1024, height: 768))
    }
}

extension UIView {
    func asImage() -> UIImage {
        let format = UIGraphicsImageRendererFormat()
        format.scale = 1
        return UIGraphicsImageRenderer(size: self.layer.frame.size, format: format).image { context in
            self.drawHierarchy(in: self.layer.bounds, afterScreenUpdates: true)
        }
    }
}


extension View {
    func asImage(size: CGSize) -> UIImage {
        let controller = UIHostingController(rootView: self)
        controller.view.bounds = CGRect(origin: .zero, size: size)
        let image = controller.view.asImage()
        return image
    }
}
